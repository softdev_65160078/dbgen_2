/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.newgen.dbgen_2;

import com.newgen.dbgen_2.dao.UserDao;
import com.newgen.dbgen_2.dbhelper.DBHelper;
import com.newgen.dbgen_2.model.User;

/**
 *
 * @author Phattharaphon
 */
public class TestUserDao {
    public static void main(String[] args) {
        UserDao userDao = new UserDao();
         for(User u: userDao.getAll()){
            System.out.println(u);
        }
//        user1.setGender("F");
//        userDao.update(user1);
//        User updateUser = userDao.get(user1.getId());
//        System.out.println(updateUser);
        
//        userDao.delete(user1);
//        for(User u: userDao.getAll()){
//            System.out.println(u);
//        }
          for(User u:userDao.getAll("user_name like 'u%' ", " user_name asc,user_gender desc")){
             System.out.println(u);
        }
        
        DBHelper.close();
    }
}
